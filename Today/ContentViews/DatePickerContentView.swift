//
//  DatePickerContentView.swift
//  Today
//
//  Created by rta on 12/17/23.
//

import UIKit

class DatePickerContentView: UIView,UIContentView {
    
    struct Configuration: UIContentConfiguration {
        var date = Date.now
        var onChange: (Date) -> Void = { _ in}
        func makeContentView () -> UIView & UIContentView {
            return DatePickerContentView(self)
        }
    }
    
    let datePicker = UIDatePicker()
    
    var configuration: UIContentConfiguration {
        didSet {
            configure(configuration: configuration)
        }
    }
    
    init(_ configuration: UIContentConfiguration) {
        self.configuration = configuration
        super.init(frame: .zero)
        addPinnedSubview(datePicker)
        datePicker.addTarget(self, action: #selector(didPick(_:)), for: .valueChanged)
        datePicker.preferredDatePickerStyle = .inline
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(code) has not been implement")
    }
    
    func configure(configuration: UIContentConfiguration) {
        guard let configuration = configuration as? Configuration else {return}
        datePicker.date = configuration.date
    }
    
    @objc private func didPick(_ sender: UIDatePicker) {
        guard let configuration = configuration as? Configuration else {return}
        configuration.onChange(sender.date)
    }
    
}

extension UICollectionViewListCell {
    func datePickerCofiguration() -> DatePickerContentView.Configuration {
        DatePickerContentView.Configuration()
    }
}
