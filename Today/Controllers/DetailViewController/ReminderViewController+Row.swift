//
//  ReminderViewController+Row.swift
//  Today
//
//  Created by rta on 12/16/23.
//

import UIKit

extension ReminderViewController {
    
    enum Row: Hashable {
        case header(String)
        case date
        case notes
        case time
        case title
        case editableDat(Date)
        case editableText(String?)
        var imageName: String? {
            switch self {
            case .date: return "calendar.circle"
            case .notes: return "square.and.pencil"
            case .time: return "clock"
            default: return nil
            }
        }
        
        var image: UIImage? {
            guard let imageName = imageName else { return nil }
            let cofiguration = UIImage.SymbolConfiguration(textStyle: .headline)
            return UIImage(systemName: imageName,withConfiguration: cofiguration)
        }
        
        var textStyle: UIFont.TextStyle {
            switch self {
            case .title: return .headline
            default: return .subheadline
            }
        }
        
    }

}
